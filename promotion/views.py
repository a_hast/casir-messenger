# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db import IntegrityError
from django import forms

from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User

from members.forms import AuthenticationForm, SignupForm
from members.models import Member

def index(request):

    messaging_url = "/dashboard/index/"

    authentication_form = AuthenticationForm(request, prefix='authentication')
    signup_form         = SignupForm(request.POST, prefix = 'signup')
    
    if request.method == "POST":
        user = None
        if "login" in request.POST:
            authentication_form = AuthenticationForm(request, data=request.POST, prefix='authentication')
            if authentication_form.is_valid():
                # Okay, security check complete. Get the user.
                user = authentication_form.get_user()
                
            if user is not None:
                auth_login(request, authentication_form.get_user())
                return HttpResponseRedirect(messaging_url)
                
        elif "register" in request.POST:
            signup_form = SignupForm(request.POST, prefix = 'signup')
            
            if signup_form.is_valid():
                user = Member()
                username = signup_form.cleaned_data['username']
                mail     = signup_form.cleaned_data['mail']
                password = signup_form.cleaned_data['password']
                
                user.user = User.objects.create_user(username, mail, password)
                user.save()

    context = {
        'authentication_form' : authentication_form,
        'signup_form'         : signup_form
    }
    
    return render(request, "promotion/index.html", context)
